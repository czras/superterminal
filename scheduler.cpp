#include "scheduler.h"

Scheduler::Scheduler(int child_count, QObject *parent) :
    QObject(parent)
{
  children.reserve(child_count);

  for (int i = 0; i < child_count; ++i)
    startChild(i);
}

Scheduler::~Scheduler()
{
  destroyChildren();
}

void Scheduler::queueCommand(QString cmd)
{
  commands.push_back(cmd);
  scheduleCommand(getAvailableChild());
}

void Scheduler::scheduleCommand(int id)
{
  if (canScheduleCommand(id))
    {
      children[id]->runCommand(commands.front());
      commands.pop_front();
    }
}

void Scheduler::destroyChildren()
{
  while (children.size())
    {
      delete children.back();
      children.pop_back();
    }
}

void Scheduler::startChild(int id)
{
  children.push_back(new ChildWindow(id));
  children.back()->show();
  QObject::connect(children.back(), SIGNAL(commandFinished(int)), this, SLOT(scheduleCommand(int)));
}

int Scheduler::getAvailableChild()
{
  int id;
  for (id = 0; id < (int)children.size(); ++id)
    if (!children[id]->isBusy())
      break;

  return id;
}

bool Scheduler::canScheduleCommand(int id)
{
  return !commands.empty() &&
         0 <= id && id < (int)children.size();
}
