#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "terminalwidget.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  TerminalWidget *terminal(new TerminalWidget);
  setCentralWidget(terminal); // Qt takes owneship

  QObject::connect(terminal, SIGNAL(commandEntered(QString)), this, SLOT(executeCommand(QString)));
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::executeCommand(QString cmd)
{
  if (!cmd.isEmpty())
    emit commandEntered(cmd);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
  emit closed();
  QMainWindow::closeEvent(event);
}
