#include "childwindow.h"
#include "ui_childwindow.h"

#include <QPlainTextEdit>

#include <vector>

namespace
{
  const int MAX_LINES = 10000;

  std::vector<QString> PROCESS_ERROR_DESCRIPTION =
  {
    "failed to start (not found or no permission to execute)",
    "crash",
    "synchronization timeout",
    "cannot read output",
    "cannot write input",
    "unknown"
  };
}

ChildWindow::ChildWindow(int id, QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::ChildWindow),
  display(new QPlainTextEdit()),
  id(id),
  prompt("> ")
{
  ui->setupUi(this);
  setWindowTitle();

  display->setReadOnly(true);
  display->setMaximumBlockCount(MAX_LINES);
  setCentralWidget(display); // Qt takes owneship

  subprocess.setProcessChannelMode(QProcess::MergedChannels);

  QObject::connect(&subprocess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(subprocessFinished(int, QProcess::ExitStatus)));
  QObject::connect(&subprocess, SIGNAL(readyReadStandardOutput()), this, SLOT(subprocessOutput()));
  QObject::connect(&subprocess, SIGNAL(error(QProcess::ProcessError)), this, SLOT(subprocessError(QProcess::ProcessError)));
}

ChildWindow::~ChildWindow()
{
  // NOTE: block signal emission to do not emit commandFinished at subprocess termination
  blockSignals(true);
  subprocess.terminate();
  subprocess.waitForFinished();

  delete ui;
}

bool ChildWindow::isBusy()
{
  return subprocess.state() != QProcess::NotRunning;
}

void ChildWindow::runCommand(QString cmd)
{
  setWindowTitle(QString("command: %1").arg(cmd));
  display->appendPlainText(prompt + cmd);
  subprocess.start(cmd);
}

void ChildWindow::closeEvent(QCloseEvent *event)
{
  Q_UNUSED(event);
}

void ChildWindow::subprocessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
  Q_UNUSED(exitCode);
  Q_UNUSED(exitStatus);

  readyForCommand();
}

void ChildWindow::subprocessError(QProcess::ProcessError e)
{
  display->appendPlainText(QString("An error happened: %1 (code: %2)").arg(PROCESS_ERROR_DESCRIPTION[e]).arg(e));

  readyForCommand();
}

void ChildWindow::subprocessOutput()
{
  display->appendPlainText(QString::fromUtf8(subprocess.readAll().data()));
  display->ensureCursorVisible();
}

void ChildWindow::readyForCommand()
{
  setWindowTitle();
  emit commandFinished(id);
}

void ChildWindow::setWindowTitle(QString suffix)
{
  QMainWindow::setWindowTitle(QString("SubProcess id: %1 %2").arg(id).arg(suffix));
}
