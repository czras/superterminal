#ifndef TERMINALWIDGET_H
#define TERMINALWIDGET_H

#include <QPlainTextEdit>

class TerminalWidget : public QPlainTextEdit
{
  Q_OBJECT

  /**
   * Text edit widget which emulates a terminal in a basic way.
   *
   * Has a prompt, keeps command history, controls key events
   * to delimit cursor movement.
   */

public:
  explicit TerminalWidget(QWidget *parent = 0);

signals:
  void commandEntered(QString cmd);

public slots:

protected:
  virtual void keyPressEvent(QKeyEvent* event);

private:
  void replaceCommand(const QString &text);
  QString getCommand();
  void addCommandToHistory(QString cmd);
  int getPromptPosition(const QTextCursor &c);

private:
  QString prompt;
  QStringList history;
  QStringList::iterator history_cursor;
};

#endif // TERMINALWIDGET_H
