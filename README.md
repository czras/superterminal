# README #

## What is SuperTerminal? ##

* A basic __terminal__ which _do not restrict you_ to wait for your commands to finish (for a limit at least)
* The user can enter system commands (to use like bash run a command as `bash -c "<COMMAND>"`) in a main window
* The commands are running as subprocesses and their output is displayed on separate windows

## How to build? ##

* To compile and link SuperTerminal only the _Qt5 Core and GUI libraries_ necessary and a _C++11 compatible compiler_
* If they are installed, simply run `qmake && make` in the project directory then run `./SuperTerminal <subprocess count>` to start the application
* Alternatively, if you have QtCreator installed, you can load the project `SuperTerminal.pro` and build/run from there

## How to use? ##

* Specify how many subprocesses could be run parallel from the command line (usage: `SuperTerminal <subprocess count>`) then enjoy typing commands
* In QtCreator command line arguments can be specified at the "Projects/Run settings" menu
* If you finished with your work simply close the main window, this will terminate all the running subprocesses and close all windows

## Known issues / What is not working ##

* Tested on __Linux__ but should work on any other Qt supported platform (Windows, Mac)
* Console applications like `mc` and `nano`, are not using the standard output but the alternate screen of the terminal. These are _not supported_
* Applications expecting input on the standard input are _not supported_
* Parallel execution of lots of applications generating heavy stdout data (like `ls -lR /`) will _downgrade UI responsivity_