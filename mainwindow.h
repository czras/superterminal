#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

  /**
   * Main window which allows command input via a TerminalWidget.
   */

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

signals:
  void commandEntered(QString cmd);
  void closed();

public slots:
  void closeEvent(QCloseEvent* event);

private slots:
  void executeCommand(QString cmd);

private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
