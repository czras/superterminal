#ifndef CHILDWINDOW_H
#define CHILDWINDOW_H

#include <QMainWindow>
#include <QProcess>

namespace Ui {
  class ChildWindow;
}

class QPlainTextEdit;

class ChildWindow : public QMainWindow
{
  Q_OBJECT

  /**
   * Child window which executes commands as a subprocess.
   *
   * Starts the subprocess for running the command.
   * Reads its output while running.
   * Terminates subprocess on destruction.
   *
   * NOTE: Cannot be closed via a closeEvent, the main application
   * should destroy the object to be closed.
   */

public:
  explicit ChildWindow(int id, QWidget *parent = 0);
  ~ChildWindow();

  bool isBusy();
  void runCommand(QString cmd);

signals:
  void commandFinished(int id);

public slots:
  void closeEvent(QCloseEvent* event);

private slots:
  void subprocessFinished(int exitCode, QProcess::ExitStatus exitStatus);
  void subprocessError(QProcess::ProcessError e);
  void subprocessOutput();

private:
  void readyForCommand();
  void setWindowTitle(QString suffix = "");

private:
  Ui::ChildWindow *ui;
  QPlainTextEdit *display;
  int id;
  QString prompt;
  QProcess subprocess;
};

#endif // CHILDWINDOW_H
