#include "terminalwidget.h"

#include <QTextBlock>
#include <QScrollBar>

namespace
{
  const int HISTORY_MAX_SIZE = 100;
}

TerminalWidget::TerminalWidget(QWidget *parent) :
  QPlainTextEdit(parent),
  prompt("prompt> "),
  history(),
  history_cursor(history.begin())
{
  setFont(QFont("monospace"));
  appendPlainText(prompt);
}

void TerminalWidget::keyPressEvent(QKeyEvent *event)
{
  switch (event->key())
    {
    case Qt::Key_Up:
      if (!history.isEmpty())
        {
          replaceCommand(*history_cursor);
          if (history_cursor != history.end() - 1)
            ++history_cursor;
        }
      break;

    case Qt::Key_Down:
      replaceCommand(history_cursor == history.begin() ? "" : *--history_cursor);
      break;

    case Qt::Key_Left:
    case Qt::Key_Backspace:
      if (textCursor().positionInBlock() > prompt.size())
        QPlainTextEdit::keyPressEvent(event);
      break;

    case Qt::Key_Home:
      {
        QTextCursor c(textCursor());
        c.setPosition(getPromptPosition(c));
        setTextCursor(c);
      }
      break;

    case Qt::Key_PageUp:
      verticalScrollBar()->setValue(verticalScrollBar()->value() - verticalScrollBar()->pageStep());
      break;

    case Qt::Key_PageDown:
      verticalScrollBar()->setValue(verticalScrollBar()->value() + verticalScrollBar()->pageStep());
      break;

    case Qt::Key_Return:
    case Qt::Key_Enter:
      {
        QString cmd(getCommand());

        addCommandToHistory(cmd);
        emit commandEntered(cmd);
      }

      appendPlainText(prompt);
      break;

    default:
      QPlainTextEdit::keyPressEvent(event);
      break;
    }
}

void TerminalWidget::replaceCommand(const QString &text)
{
  QTextCursor cursor(textCursor());
  cursor.setPosition(getPromptPosition(cursor));
  cursor.setPosition(cursor.block().position() + cursor.block().length() - 1, QTextCursor::KeepAnchor);
  cursor.removeSelectedText();
  cursor.insertText(text);
}

QString TerminalWidget::getCommand()
{
  return textCursor().block().text().mid(prompt.size());
}

void TerminalWidget::addCommandToHistory(QString cmd)
{
  while (history.size() >= HISTORY_MAX_SIZE)
    history.removeLast();

  history.insert(history.begin(), cmd);
  history_cursor = history.begin();
}

int TerminalWidget::getPromptPosition(const QTextCursor &c)
{
  return c.block().position() + prompt.size();
}
