#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "childwindow.h"

#include <QObject>

#include <deque>
#include <vector>

class Scheduler : public QObject
{
  Q_OBJECT

  /**
   * Scheduler to control ChildWindows.
   *
   * Queues/schedules commands, starts/closes children.
   */

public:
  explicit Scheduler(int child_count, QObject *parent = 0);
  ~Scheduler();

public slots:
  void queueCommand(QString cmd);
  void scheduleCommand(int id);
  void destroyChildren();

protected:
  void startChild(int id);
  int getAvailableChild();
  bool canScheduleCommand(int id);

private:
  std::deque<QString> commands;
  std::vector<ChildWindow *> children;
};

#endif // SCHEDULER_H
