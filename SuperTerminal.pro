#-------------------------------------------------
#
# Project created by QtCreator 2014-07-14T13:39:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SuperTerminal
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    terminalwidget.cpp \
    scheduler.cpp \
    childwindow.cpp

HEADERS  += mainwindow.h \
    terminalwidget.h \
    scheduler.h \
    childwindow.h

FORMS    += mainwindow.ui \
    childwindow.ui
