#include "mainwindow.h"
#include "scheduler.h"

#include <QApplication>
#include <QDebug>

void usage(const char *reason)
{
  qCritical() << reason;
  qCritical() << "Usage: SuperTerminal <subprocess count>";
}

int get_subprocess_count(QStringList args)
{
  if (args.size() < 2)
    {
      usage("Command line argument required!");
      return 0;
    }

  bool success = false;
  int count = args.at(1).toInt(&success);

  if (!success || count < 1)
    {
      usage("Subprocess count should be a positive non-zero integer!");
      return 0;
    }

  return count;
}

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  int subprocess_count(get_subprocess_count(app.arguments()));

  if (subprocess_count < 1)
    return 1;

  Scheduler scheduler(subprocess_count);

  MainWindow main_window;

  QObject::connect(&main_window, SIGNAL(commandEntered(QString)), &scheduler, SLOT(queueCommand(QString)));
  QObject::connect(&main_window, SIGNAL(closed()), &scheduler, SLOT(destroyChildren()));

  main_window.show();

  return app.exec();
}
